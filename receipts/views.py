from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceiptForm, CreateCategoryForm, CreateAccountForm

# Create your views here.
@login_required
def home(request):
    list_of_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": list_of_receipts,
    }
    return render(request, "receipts/home.html", context)


# Feature 11 - Create Receipt View
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create_receipt.html", context)


# Feature 12 - List Views
def show_categories(request):
    categories_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories_list": categories_list
    }
    return render(request, "receipts/categories.html", context)


# Feature 12 - List Views
def show_accounts(request):
    accounts_list = Account.objects.filter(owner=request.user)
    context = {
        "accounts_list": accounts_list
    }
    return render(request, "receipts/accounts.html", context)


# Feature 13 - Create Category View
@login_required
def create_category(request):
    if request.method == 'POST':
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CreateCategoryForm()

    context = {
        "form": form
    }
    return render(request, "receipts/create_category.html", context)


# Feature 14 - Create Account View
@login_required
def create_account(request):
    if request.method == 'POST':
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()

    context = {
        "form": form
    }
    return render(request, "receipts/create_account.html", context)
